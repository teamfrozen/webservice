import {createStore, createEvent} from 'effector';
import {useStore} from 'effector-react';

export interface NotificatorConfig {
  message: string;
  variant: 'success' | 'error';
}

export const notifications = createStore<NotificatorConfig[]>([]);

export const notifySuccess = createEvent<string>();
notifications.on(notifySuccess, (state, message) => [
  ...state,
  {message, variant: 'success'},
]);

export const notifyError = createEvent<string>();
notifications.on(notifyError, (state, message) => [
  ...state,
  {message, variant: 'error'},
]);

export const resetNotifications = createEvent<void>();
notifications.reset(resetNotifications);

export const useNotifications = () => useStore(notifications);
