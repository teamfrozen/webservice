import {createEffect, createEvent, createStore} from 'effector';
import {withNotifications, withSetState} from '../utils/store';
import {projectApi} from '../services/projectApi';
import {useStore} from 'effector-react';
import {createInitRequestState, RequestState} from '../utils/request';

export interface User {
  token: string;
  data: {
    id: string;
    firstName?: string;
    login?: string;
    fridge: FridgeProduct[];
    products: Product[];
    productTypes: ProductType[];
    recommendations: Recommendation[];
  };
}

export interface Recommendation {
  name: string;
  difficulty: string;
  hours: number;
  minutes: number;
  products: string;
  steps: string;
  imagePath: string;
  recipe_id: string;
}

export interface ProductType {
  productTypeId: string;
  productTypeName: string;
}

export interface Product {
  productId: string;
  productName: string;
}

export interface FridgeProduct {
  productInFridgeId: string;
  depositDate: string;
  expirationDate: string;
  productName: string;
  productId: string;
  quantity: number;
  productTypeId: string;
  productTypeName: string;
  productImagePath: string;
  measureName: string;
  isFavorite: boolean;
}

export const userState = createStore<RequestState<User>>(
  createInitRequestState(),
);

export const authByToken = createEffect(projectApi.authUserByToken);
withSetState(userState, authByToken, ({response}) => response);

export const authUserByCredentials = createEffect(
  projectApi.authUserByCredentials,
);
withSetState(userState, authUserByCredentials, ({response}) => response);
withNotifications(authUserByCredentials);

export const signUserByCredentials = createEffect(
  projectApi.signUserByCredentials,
);
withSetState(userState, signUserByCredentials, ({response}) => response);
withNotifications(signUserByCredentials);

export const signUserByTelegram = createEffect(projectApi.signUserByTelegram);
withSetState(userState, signUserByTelegram, ({response}) => response);
withNotifications(signUserByTelegram);

export const resetUserState = createEvent();
userState.on(resetUserState, createInitRequestState);

export const addProductToFridge = createEffect(projectApi.addProductToFridge);
withSetState(userState, addProductToFridge, ({state, response}) => {
  if (!state || !response) return state as any;

  return {
    ...state,
    data: {
      ...state.data,
      fridge: response,
    },
  };
});
withNotifications(addProductToFridge);

export const removeProductFromFridge = createEffect(
  projectApi.removeProductFromFridge,
);
withSetState(userState, removeProductFromFridge, ({state, response}) => {
  if (!state || !response) return state as any;

  return {
    ...state,
    data: {
      ...state.data,
      fridge: response,
    },
  };
});
withNotifications(removeProductFromFridge);

export const fetchProductsByProductTypeId = createEffect(
  projectApi.getProductsByProductTypeId,
);
withSetState(userState, fetchProductsByProductTypeId, ({state, response}) => {
  if (!state || !response) return state as any;

  return {
    ...state,
    data: {
      ...state.data,
      products: response,
    },
  };
});

export const useUserState = () => useStore(userState);
