import s from './SpaceModal.module.scss';

import {useForm} from 'react-hook-form';
import {Button, Modal, Paper, Typography, TextField} from '@mui/material';
import {useSnackbar} from 'notistack';
import {useState} from 'react';

import c from './config.json';

interface Props {
  className?: string;
}

export function SpaceModal({className}: Props): JSX.Element {
  const {enqueueSnackbar} = useSnackbar();

  const {register, handleSubmit, reset} = useForm();

  const [isOpen, setOpen] = useState(false);

  const handleChangeIsOpen = () => {
    setOpen((prev) => !prev);
  };

  const onSubmit = (data: any) => {
    console.log(data);
    reset();
    enqueueSnackbar('Success', {variant: 'success'});
  };

  return (
    <>
      <Button
        variant="outlined"
        className={className}
        onClick={handleChangeIsOpen}
      >
        {c.SpaceModal.buttonText}
      </Button>
      <Modal
        open={isOpen}
        onClose={handleChangeIsOpen}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <section className={s.root}>
          <Paper elevation={24} className={s.container}>
            <Typography variant="h5" className={s.title}>
              {c.SpaceModal.title}
            </Typography>
            <form onSubmit={handleSubmit(onSubmit)} className={s.form}>
              <TextField
                required
                variant="outlined"
                label="login"
                {...register('login')}
              />
              <TextField
                required
                variant="outlined"
                label="role"
                {...register('role')}
              />
              <Button variant="outlined" type="submit">
                {c.SpaceModal.submitButtonText}
              </Button>
            </form>
          </Paper>
        </section>
      </Modal>
    </>
  );
}
