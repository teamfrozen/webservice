import s from './FoodModal.module.scss';

import {useForm} from 'react-hook-form';
import {
  Button,
  Modal,
  Paper,
  Typography,
  TextField,
  Autocomplete,
} from '@mui/material';
import {
  useUserState,
  addProductToFridge,
  fetchProductsByProductTypeId,
} from '../../../store/user';
import {MobileDatePicker} from '@mui/lab';
import React, {useEffect, useState} from 'react';

import c from './config.json';

interface Props {
  className?: string;
}

export function FoodModal({className}: Props): JSX.Element | null {
  const userState = useUserState();

  const {register, handleSubmit, reset} = useForm();

  const [isOpen, setOpen] = useState(false);

  const [expirationDate, setValue] = useState(new Date());

  const [productType, setProductType] = useState('');

  useEffect(() => {
    if (productType) {
      const productTypeId = userState.data?.data.productTypes.find(
        (i) => i.productTypeName === productType,
      );

      fetchProductsByProductTypeId(productTypeId?.productTypeId as string);
    }
  }, [productType, userState.data?.data.productTypes]);

  const handleChange = (newValue: any) => {
    setValue(newValue);
  };

  const handleChangeIsOpen = () => {
    setOpen((prev) => !prev);
  };

  const onSubmit = async (data: any) => {
    const product = userState.data?.data.products.find(
      (i) => i.productName === data.type,
    );

    await addProductToFridge({
      productName: data.productName,
      productTypeId: product?.productId as string,
      expirationDate: expirationDate.toISOString(),
      quantity: data.quantity,
    });

    reset();
    setOpen(false);
  };

  if (!userState.data) {
    return null;
  }

  const optionsType = userState.data.data.productTypes.map(
    ({productTypeName}) => productTypeName,
  );

  const optionsProductName = userState.data.data.products.map(
    ({productName}) => productName,
  );

  return (
    <>
      <Button
        variant="outlined"
        className={className}
        onClick={handleChangeIsOpen}
      >
        {c.FoodModal.buttonText}
      </Button>
      <Modal
        open={isOpen}
        onClose={handleChangeIsOpen}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <section className={s.root}>
          <Paper elevation={24} className={s.container}>
            <Typography variant="h5" className={s.title}>
              {c.FoodModal.title}
            </Typography>
            <form onSubmit={handleSubmit(onSubmit)} className={s.form}>
              <Autocomplete
                disablePortal
                options={optionsType}
                onChange={(_, newValue: any) => {
                  setProductType(newValue);
                }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    required
                    variant="outlined"
                    label="type"
                  />
                )}
              />
              <Autocomplete
                disablePortal
                options={optionsProductName}
                renderInput={(params) => (
                  <TextField
                    required
                    {...params}
                    {...register('productName')}
                    variant="outlined"
                    label="productName"
                  />
                )}
              />
              <TextField
                type="number"
                variant="outlined"
                label="quantity"
                {...register('quantity')}
              />
              <MobileDatePicker
                label="expirationDate"
                inputFormat="MM/dd/yyyy"
                value={expirationDate}
                onChange={handleChange}
                renderInput={(params: any) => <TextField {...params} />}
              />
              <Button variant="outlined" type="submit">
                {c.FoodModal.submitButtonText}
              </Button>
            </form>
          </Paper>
        </section>
      </Modal>
    </>
  );
}
