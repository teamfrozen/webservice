import s from './CardRecommendation.module.scss';

import {Recommendation} from '@store/user';
import {Paper, Typography} from '@mui/material';

export function CardRecommendation({
  name,
  hours,
  minutes,
  products,
  steps,
}: Recommendation): JSX.Element {
  const renderProduct = (str: string) => (
    <Typography variant="body1">{str}</Typography>
  );

  const renderStep = (str: string) => (
    <>
      <br />
      <Typography variant="body1">{str}</Typography>
      <br />
    </>
  );

  return (
    <Paper className={s.root}>
      <Typography variant="h3">{name}</Typography>
      <br />
      <Typography variant="h4">Time</Typography>
      <Typography variant="h5">{`${
        hours.toString().length === 1 ? `0${hours}` : hours
      }:${
        minutes.toString().length === 1 ? `0${minutes}` : minutes
      }`}</Typography>
      <br />
      <Typography variant="h3">Products:</Typography>
      <br />
      {products.split(',').map(renderProduct)}
      <br />
      <Typography variant="h3">Steps:</Typography>
      <br />
      {steps.split(',').map(renderStep)}
    </Paper>
  );
}
