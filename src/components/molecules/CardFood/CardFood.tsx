import s from './CardFood.module.scss';

import {IconButton, Paper, Typography} from '@mui/material';
import {Delete} from '@mui/icons-material';
import faker from 'faker';

import {Image} from '@atomic/atoms/Image/Image';
import {randomIntFromInterval} from '@utils/number';
import {FridgeProduct, removeProductFromFridge} from '../../../store/user';

import c from './config.json';

faker.setLocale('ru');

interface Props {
  title: string;
  expirationDate: string;
  productId: string;
}

export function CardFood(props: FridgeProduct): JSX.Element {
  return (
    <CardProductBase
      title={props.productName}
      expirationDate={props.expirationDate}
      productId={props.productInFridgeId}
    />
  );
}

function CardProductBase({
  title,
  expirationDate,
  productId,
}: Props): JSX.Element {
  const handleRemove = () => {
    removeProductFromFridge(productId);
  };

  const {year, month, day} = getDate(expirationDate);

  return (
    <Paper className={s.root} style={{background: getRandomGradient()}}>
      <div className={s.image}>
        <Image
          type="internal"
          src={`/food/food_${randomIntFromInterval(1, 12)}.jpg`}
        />
      </div>

      <div className={s.body}>
        <div className={s.controllers}>
          <Typography color="gray" variant="h5" mb={0}>
            {title}
          </Typography>
          <div>
            {/* <IconButton onClick={handleUpdate}>
              <ModeEditOutlineTwoTone />
            </IconButton> */}
            <IconButton onClick={handleRemove}>
              <Delete />
            </IconButton>
          </div>
        </div>
        {/* <Typography color="gray" variant="subtitle1" className={s.subtitle}>
          {subtitle}
        </Typography> */}
      </div>

      <div className={s.footer}>
        <Image
          type="internal"
          src={c.CardFood.leftFooterIcon}
          className={s.leftFooterIcon}
        />
        <Typography color="gray" variant="subtitle2">
          {`${year}-${month}-${day}`}
        </Typography>
      </div>
    </Paper>
  );
}

function getRandomGradient() {
  const gradients = [
    'linear-gradient(to right, #ffefba, #ffffff)',
    'linear-gradient(to right, #a6ffad, #ffffff)',
    'linear-gradient(to right, #ffa6a6, #ffffff)',
  ];

  return gradients[randomIntFromInterval(0, 2)];
}

function getDate(string: string) {
  const [year, month, day, hour, minute, second, millisecond] = new Date(string)
    .toISOString()
    .split(/[^0-9]/)
    .slice(0, -1);

  return {year, month, day, hour, minute, second, millisecond};
}
