import {resetUserState, useUserState} from '@store/user';
import {isRequestStatusSuccess} from '@utils/request';
import {LogoutOutlined, InputOutlined} from '@mui/icons-material';
import {clearStorage} from '@utils/storage';
import {Link} from '@atomic/atoms/Link/Link';
import {AppPath} from '@constants/AppConfig';
import {IconButton} from '@mui/material';
import {useRouter} from 'next/router';

export function AuthButton(): JSX.Element | null {
  const userState = useUserState();
  const router = useRouter();

  const handleClick = () => {
    resetUserState();
    clearStorage();

    router.push(AppPath.Auth);
  };

  if (isRequestStatusSuccess(userState)) {
    return (
      <IconButton onClick={handleClick}>
        <LogoutOutlined />
      </IconButton>
    );
  }

  return (
    <Link type="internal" href={AppPath.Auth}>
      <IconButton onClick={handleClick}>
        <InputOutlined />
      </IconButton>
    </Link>
  );
}
