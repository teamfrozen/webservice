import {Header} from '@atomic/organisms/Header/Header';
import {Footer} from '@atomic/organisms/Footer/Footer';
import {RecommendationsList} from '@atomic/organisms/RecommendationsList/RecommendationsList';
import {useRedirectUauthorizedUser} from '@hooks/useRedirectUnauthorizedUser';
import {useStartAuthorizeUser} from '@hooks/useStartAuthorizeUser';
import {authByToken} from '@store/user';
import {useEffect} from 'react';

import s from './RecommendationsBody.module.scss';

export function RecommendationsBody(): JSX.Element {
  useStartAuthorizeUser();
  useRedirectUauthorizedUser();

  useEffect(() => {
    authByToken();
  }, []);

  return (
    <>
      <Header />
      <main className={s.main}>
        <RecommendationsList />
      </main>
      <Footer />
    </>
  );
}
