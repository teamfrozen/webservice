import {PageHead} from '../../templates/PageHead/PageHead';

import c from './config.json';

export function RecommendationsHead(): JSX.Element {
  return <PageHead {...c.RecommendationsHead} />;
}
