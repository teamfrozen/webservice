import {Header} from '@atomic/organisms/Header/Header';
import {Footer} from '@atomic/organisms/Footer/Footer';
import {FoodList} from '@atomic/organisms/FoodList/FoodList';
import {useRedirectUauthorizedUser} from '@hooks/useRedirectUnauthorizedUser';
import {useStartAuthorizeUser} from '@hooks/useStartAuthorizeUser';

import s from './FridgeBody.module.scss';

export function FridgeBody(): JSX.Element {
  useStartAuthorizeUser();
  useRedirectUauthorizedUser();

  return (
    <>
      <Header />
      <main className={s.main}>
        <FoodList />
      </main>
      <Footer />
    </>
  );
}
