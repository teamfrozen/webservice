import s from './AuthBody.module.scss';

import {Grid} from '@mui/material';

import {Header} from '@atomic/organisms/Header/Header';
import {Footer} from '@atomic/organisms/Footer/Footer';
import {AuthForm} from '@atomic/organisms/AuthForm/AuthForm';
import {useStartAuthorizeUser} from '@hooks/useStartAuthorizeUser';
import {useRedirectAuthorizedUser} from '@hooks/useRedirectAuthorizedUser';

export function AuthBody(): JSX.Element {
  useStartAuthorizeUser();
  useRedirectAuthorizedUser();

  return (
    <>
      <Header />
      <div className={s.root}>
        <Grid p={2}>
          <AuthForm />
        </Grid>
        <Footer />
      </div>
    </>
  );
}
