import {PageHead} from '@atomic/templates/PageHead/PageHead';

import c from './config.json';

export function AuthHead(): JSX.Element {
  return <PageHead {...c.AuthHead} />;
}
