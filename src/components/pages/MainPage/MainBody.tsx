import s from './MainBody.module.scss';

import {Header} from '@atomic/organisms/Header/Header';
import {Welcome} from '@atomic/molecules/Welcome/Welcome';
import {Benefits} from '@atomic/organisms/Benefits/Benefits';
import {useStartAuthorizeUser} from '@hooks/useStartAuthorizeUser';
import {Footer} from '@atomic/organisms/Footer/Footer';
import {JoinUs} from '@atomic/organisms/JoinUs/JoinUs';

import {Grid} from '@mui/material';

export function MainBody(): JSX.Element {
  useStartAuthorizeUser();

  return (
    <main className={s.root}>
      <Header />
      <Grid container justifyContent="center" p={4}>
        <Welcome />
      </Grid>
      <Benefits />
      <Grid container justifyContent="center" p={4}>
        <JoinUs />
      </Grid>
      <Footer />
    </main>
  );
}
