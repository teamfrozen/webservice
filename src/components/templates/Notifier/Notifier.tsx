import {useNotifications, resetNotifications} from '@store/notifications';
import React, {useEffect} from 'react';
import {useSnackbar} from 'notistack';

export const Notifier: React.FC = () => {
  const {enqueueSnackbar} = useSnackbar();

  const notifications = useNotifications();

  useEffect(() => {
    notifications.forEach(({message, variant}) => {
      enqueueSnackbar(message, {variant});
    });

    resetNotifications();
  }, [enqueueSnackbar, notifications]);

  return null;
};
