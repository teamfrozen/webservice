import s from './JoinUs.module.scss';

import {Link} from '@atomic/atoms/Link/Link';
import {Image} from '@atomic/atoms/Image/Image';
import {Button, Typography} from '@mui/material';
import {isRequestStatusSuccess} from '@utils/request';
import {AppPath} from '@constants/AppConfig';
import {useUserState} from '@store/user';

import c from './config.json';

export function JoinUs(): JSX.Element {
  const userState = useUserState();

  const buildLinkHref = () => {
    if (isRequestStatusSuccess(userState)) {
      return AppPath.Fridge;
    } else {
      return AppPath.Auth;
    }
  };

  const buildButtonTitle = () => {
    if (isRequestStatusSuccess(userState)) {
      return c.JoinUs.button.toFridge;
    } else {
      return c.JoinUs.button.toAuth;
    }
  };

  return (
    <div className={s.root}>
      <Typography variant="h2" textAlign="center" mb={4} fontWeight="500">
        {c.JoinUs.title}
      </Typography>
      <div className={s.join}>
        <Image
          type="internal"
          src={c.JoinUs.image.src}
          className={s.leftSideImage}
        />
        <div className={s.rightSideText}>
          <Typography variant="h5" fontWeight="400" className={s.text1}>
            {c.JoinUs.join.title}
          </Typography>
          <Typography className={s.text2}>
            {c.JoinUs.join.description}
          </Typography>
          <Link type="internal" href={buildLinkHref()} className={s.button}>
            <Button variant="outlined">{buildButtonTitle()}</Button>
          </Link>
        </div>
      </div>
    </div>
  );
}
