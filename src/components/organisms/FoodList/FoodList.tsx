import s from './FoodList.module.scss';

import {CardFood} from '@atomic/molecules/CardFood/CardFood';
import {FoodModal} from '@atomic/molecules/FoodModal/FoodModal';
import {SpaceModal} from '@atomic/molecules/SpaceModal/SpaceModal';
import {FridgeProduct, useUserState} from '../../../store/user';
import {Typography, Divider} from '@mui/material';

import c from './config.json';

export function FoodList(): JSX.Element {
  const userState = useUserState();

  const renderCardFood = (product: FridgeProduct) => <CardFood {...product} />;

  return (
    <>
      <section>
        <Typography variant="h2" className={s.title}>
          {c.FoodCardsList.title}
        </Typography>
        <Divider orientation="horizontal" className={s.divider} flexItem>
          {c.FoodCardsList.dividerText}
        </Divider>
      </section>
      <section className={s.controls}>
        <FoodModal className={s.modalButton} />
        <SpaceModal className={s.modalButton} />
      </section>
      <section className={s.cards}>
        {userState.data && userState.data.data.fridge.map(renderCardFood)}
      </section>
    </>
  );
}
