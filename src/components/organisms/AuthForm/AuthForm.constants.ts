import {TextFieldProps} from '@mui/material';

export enum AuthFormType {
  SignUp = 'signUp',
  Login = 'login',
}

export enum SignUpFormField {
  Login = 'login',
  FirstName = 'firstName',
  PasswordOriginal = 'passwordOriginal',
  PasswordConfirm = 'passwordConfirm',
}

export const AUTH_FORM_FIELDS_SIGN_UP: Record<SignUpFormField, TextFieldProps> =
  {
    [SignUpFormField.Login]: {
      label: 'Login',
      type: 'text',
      required: true,
    },
    [SignUpFormField.FirstName]: {
      label: 'First Name',
      type: 'text',
      required: true,
    },
    [SignUpFormField.PasswordOriginal]: {
      label: 'Your Password',
      type: 'password',
      required: true,
    },
    [SignUpFormField.PasswordConfirm]: {
      label: 'Confirm Your Password',
      type: 'password',
      required: true,
    },
  };

export enum LoginFormField {
  Login = 'login',
  Password = 'password',
}

export const AUTH_FORM_FIELDS_LOGIN: Record<LoginFormField, TextFieldProps> = {
  [LoginFormField.Login]: {
    label: 'Login',
    type: 'text',
    required: true,
  },
  [LoginFormField.Password]: {
    label: 'Password',
    type: 'password',
    required: true,
  },
};
