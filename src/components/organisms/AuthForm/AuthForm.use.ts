import {useState} from 'react';
import {useForm} from 'react-hook-form';
import {TelegramUser} from 'telegram-login-button';
import {
  authUserByCredentials,
  signUserByCredentials,
  signUserByTelegram,
} from '../../../store/user';
import {
  AuthFormState,
  AuthFormStateLogin,
  AuthFormStateSignUp,
} from './AuthForm';
import {
  AuthFormType,
  AUTH_FORM_FIELDS_LOGIN,
  AUTH_FORM_FIELDS_SIGN_UP,
} from './AuthForm.constants';

export function useAuthForm() {
  const {register, handleSubmit, reset} = useForm();

  const [state, setState] = useState<AuthFormState>(createStateLogin);

  const handleSwitchAuth = () => {
    reset();

    if (state.type === AuthFormType.Login) {
      return setState(createStateSignUp());
    } else {
      return setState(createStateLogin());
    }
  };

  const handleSubmitForm = handleSubmit((data: any) => {
    if (state.type === AuthFormType.Login) {
      authUserByCredentials({
        login: data.login,
        password: data.password,
      });
    } else {
      signUserByCredentials({
        login: data.login,
        password: data.passwordOriginal,
        firstName: data.firstName,
      });
    }
  });

  const handleSubmitTelegram = (user: TelegramUser) => {
    signUserByTelegram({
      telegramId: user.id.toString(),
      firstName: user.username,
    });
  };

  return {
    state,
    methods: {
      handleSubmitForm,
      handleSubmitTelegram,
      handleSwitchAuth,
      register,
    },
  };
}

function createStateLogin(): AuthFormStateLogin {
  return {
    type: AuthFormType.Login,
    fields: AUTH_FORM_FIELDS_LOGIN,
  };
}

function createStateSignUp(): AuthFormStateSignUp {
  return {
    type: AuthFormType.SignUp,
    fields: AUTH_FORM_FIELDS_SIGN_UP,
  };
}
