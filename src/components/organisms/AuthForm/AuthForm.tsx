import s from './AuthForm.module.scss';

import {Image} from '@atomic/atoms/Image/Image';
import TelegramLoginButton from 'telegram-login-button';
import {TelegramBot} from '@constants/AppConfig';
import {isRequestStatusLoading, isRequestStatusSuccess} from '@utils/request';
import {useUserState} from '@store/user';
import {useAuthForm} from './AuthForm.use';

import {
  Button,
  Paper,
  TextField,
  TextFieldProps,
  Typography,
  CircularProgress,
} from '@mui/material';

import {
  SignUpFormField,
  LoginFormField,
  AuthFormType,
} from './AuthForm.constants';

import c from './config.json';

export type AuthFormState = AuthFormStateSignUp | AuthFormStateLogin;

export interface AuthFormStateLogin {
  type: AuthFormType.Login;
  fields: Record<LoginFormField, TextFieldProps>;
}

export interface AuthFormStateSignUp {
  type: AuthFormType.SignUp;
  fields: Record<SignUpFormField, TextFieldProps>;
}

export function AuthForm(): JSX.Element {
  const userState = useUserState();

  const {state, methods} = useAuthForm();
  const {title, buttonText} = buildResources(state.type);

  const {handleSubmitForm, handleSubmitTelegram, handleSwitchAuth, register} =
    methods;

  const renderField = ([key, config]: [string, TextFieldProps]) => {
    return <TextField key={key} {...register(key)} {...config} />;
  };

  const renderButton = () => {
    if (
      isRequestStatusLoading(userState) ||
      isRequestStatusSuccess(userState)
    ) {
      return (
        <div className={s.preloader}>
          <CircularProgress />
        </div>
      );
    }

    return (
      <Button variant="outlined" type="submit">
        {c.AuthForm.submitButtonText}
      </Button>
    );
  };

  return (
    <Paper className={s.paper}>
      <Image type="internal" src={c.AuthForm.image.src} className={s.image} />
      <Typography variant="h4" className={s.title}>
        {title}
      </Typography>
      <Button
        color="secondary"
        variant="text"
        onClick={handleSwitchAuth}
        className={s.switchButton}
      >
        {buttonText}
      </Button>
      {/* форма авторизации */}
      <form onSubmit={handleSubmitForm} className={s.form}>
        {Object.entries(state.fields).map(renderField)}
        {renderButton()}
      </form>
      {/* кнопка авторизоваться через телеграмм */}
      <TelegramLoginButton
        botName={TelegramBot.SmartFridgeNotifierBot}
        dataOnauth={handleSubmitTelegram}
      />
    </Paper>
  );
}

function buildResources(type: AuthFormType) {
  if (type === AuthFormType.Login) {
    return c.AuthForm.Login;
  }

  return c.AuthForm.SignUp;
}
