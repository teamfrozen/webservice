import {Typography} from '@mui/material';
import {Recommendation, useUserState} from '@store/user';
import {CardRecommendation} from '@atomic/molecules/CardRecommendation/CardRecommendation';

import s from './RecommendationsList.module.scss';

export function RecommendationsList(): JSX.Element {
  const userState = useUserState();

  const renderRecommendation = (data: Recommendation, index: number) => {
    return <CardRecommendation key={index} {...data} />;
  };

  return (
    <>
      <section className={s.title}>
        <Typography align="center" variant="h2">
          Your recommendations
        </Typography>
      </section>
      <section className={s.cards}>
        {userState.data &&
          userState.data.data.recommendations.map(renderRecommendation)}
      </section>
    </>
  );
}
