import {useEffect} from 'react';
import {authByToken, useUserState} from '@store/user';
import {isRequestStateInit} from '@utils/request';

export function useStartAuthorizeUser() {
  const userState = useUserState();

  useEffect(() => {
    if (isRequestStateInit(userState)) {
      authByToken();
    }
  }, [userState]);
}
