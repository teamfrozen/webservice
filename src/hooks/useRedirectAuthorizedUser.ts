import {useRouter} from 'next/router';
import {useEffect} from 'react';

import {useUserState} from '@store/user';
import {AppPath} from '@constants/AppConfig';
import {isRequestStatusSuccess} from '@utils/request';

export function useRedirectAuthorizedUser() {
  const router = useRouter();
  const userState = useUserState();

  useEffect(() => {
    if (isRequestStatusSuccess(userState)) {
      router.push(AppPath.Fridge);
    }
  }, [userState, router]);
}
