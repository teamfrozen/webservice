import {useRouter} from 'next/router';
import {useEffect} from 'react';

import {useUserState} from '@store/user';
import {AppPath} from '@constants/AppConfig';
import {isRequestStateError} from '@utils/request';

export function useRedirectUauthorizedUser() {
  const router = useRouter();
  const userState = useUserState();

  useEffect(() => {
    if (isRequestStateError(userState)) {
      router.push(AppPath.Auth);
    }
  }, [userState, router]);
}
