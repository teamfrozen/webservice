import axios, {AxiosInstance, AxiosRequestConfig} from 'axios';
import {
  extractPayload,
  ResponseWithPayload,
  saveAuthTokenToStorage,
} from '@utils/services';
import {GatewayRoute, ProjectApiRoute} from '@constants/AppConfig';
import {getAuthToken} from '@utils/storage';
import {FridgeProduct, Product, User} from '@store/user';
import {replaceParam} from '../utils/string';
import noop from 'lodash/noop';

export interface SignUserByTelegramBody {
  telegramId: string;
  firstName: string;
}

export interface AuthByCredentialsBody {
  login: string;
  password: string;
}

export interface SignUserByCredentialsBody {
  login: string;
  password: string;
  firstName: string;
}

export interface AddProductToFridgeBody {
  productName: string;
  productTypeId: string;
  expirationDate: string;
  quantity?: number;
}

class ProjectApi {
  private readonly api: AxiosInstance;

  constructor() {
    this.api = axios.create({
      baseURL: 'https://brave-beaver-a053ca.netlify.app/api',
      // baseURL: 'http://localhost:8080/api'
    });
  }

  public getProductsByProductTypeId = async (
    productTypeId: string,
  ): Promise<Product[] | void> => {
    return this.api
      .post<ResponseWithPayload<FridgeProduct[]>>(
        ProjectApiRoute.Request,
        this.buildGetProductsByProductTypeId(productTypeId),
      )
      .then(extractPayload)
      .catch(noop);
  };

  private buildGetProductsByProductTypeId(
    productTypeId: string,
  ): AxiosRequestConfig {
    return {
      method: 'GET',
      url: replaceParam(GatewayRoute.GetProductsByProductTypeId, productTypeId),
      headers: this.buildAuthHeader(),
    };
  }

  public removeProductFromFridge = async (
    productId: string,
  ): Promise<FridgeProduct[] | void> => {
    return this.api
      .post<ResponseWithPayload<FridgeProduct[]>>(
        ProjectApiRoute.Request,
        this.buildRemoveProductFromFridgeParams(productId),
      )
      .then(extractPayload)
      .catch(noop);
  };

  private buildRemoveProductFromFridgeParams(
    productId: string,
  ): AxiosRequestConfig {
    return {
      method: 'DELETE',
      url: GatewayRoute.RemoveProductToFridge,
      headers: this.buildAuthHeader(),
      data: {
        productId,
      },
    };
  }

  public addProductToFridge = async (
    data: AddProductToFridgeBody,
  ): Promise<FridgeProduct[] | void> => {
    return this.api
      .post<ResponseWithPayload<FridgeProduct[]>>(
        ProjectApiRoute.Request,
        this.buildAddProductToFridgeParams(data),
      )
      .then(extractPayload)
      .catch(noop);
  };

  private buildAddProductToFridgeParams(
    data: AddProductToFridgeBody,
  ): AxiosRequestConfig {
    return {
      method: 'POST',
      url: GatewayRoute.AddProductToFridge,
      headers: this.buildAuthHeader(),
      data,
    };
  }

  public authUserByToken = async (): Promise<User> => {
    return this.api
      .post<ResponseWithPayload<User>>(
        ProjectApiRoute.Request,
        this.buildAuthUserByToken(),
      )
      .then(extractPayload)
      .then(saveAuthTokenToStorage);
  };

  private buildAuthUserByToken(): AxiosRequestConfig {
    return {
      method: 'GET',
      url: GatewayRoute.AuthUserByToken,
      headers: this.buildAuthHeader(),
    };
  }

  public authUserByCredentials = async (
    data: AuthByCredentialsBody,
  ): Promise<User> => {
    return this.api
      .post<ResponseWithPayload<User>>(
        ProjectApiRoute.Request,
        this.buildAuthUserByCredentials(data),
      )
      .then(extractPayload)
      .then(saveAuthTokenToStorage);
  };

  private buildAuthUserByCredentials(
    data: AuthByCredentialsBody,
  ): AxiosRequestConfig {
    return {
      method: 'POST',
      url: GatewayRoute.AuthUserByCredentials,
      data,
    };
  }

  public signUserByTelegram = async (
    data: SignUserByTelegramBody,
  ): Promise<User> => {
    return this.api
      .post<ResponseWithPayload<User>>(
        ProjectApiRoute.Request,
        this.buildSignUserByTelegram(data),
      )
      .then(extractPayload)
      .then(saveAuthTokenToStorage);
  };

  private buildSignUserByTelegram(
    data: SignUserByTelegramBody,
  ): AxiosRequestConfig {
    return {
      method: 'POST',
      url: GatewayRoute.SignUserByTelegram,
      data,
    };
  }

  public signUserByCredentials = async (
    data: SignUserByCredentialsBody,
  ): Promise<User> => {
    return this.api
      .post<ResponseWithPayload<User>>(
        ProjectApiRoute.Request,
        this.buildSignUserByCredentials(data),
      )
      .then(extractPayload)
      .then(saveAuthTokenToStorage);
  };

  private buildSignUserByCredentials(
    data: SignUserByCredentialsBody,
  ): AxiosRequestConfig {
    return {
      method: 'POST',
      url: GatewayRoute.SignUserByCredentials,
      data,
    };
  }

  private buildAuthHeader(): Record<string, string> {
    return {
      Authorization: `Bearer ${getAuthToken()}`,
    };
  }
}

export const projectApi = new ProjectApi();
