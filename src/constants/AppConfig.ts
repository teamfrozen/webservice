export enum AppPath {
  Main = '/',
  Auth = '/auth',
  Fridge = '/fridge',
  Recommendations = '/recommendations',
}

export enum TelegramBot {
  SmartFridgeNotifierBot = 'SmartFridgeNotifierBot',
}

export enum ProjectApiRoute {
  Request = '/request',
}

export enum GatewayRoute {
  AuthUserByToken = '/auth/me',
  SignUserByCredentials = '/auth/sign',
  SignUserByTelegram = '/auth/sign/telegram',
  AuthUserByCredentials = '/auth/login',
  AddProductToFridge = '/fridge',
  RemoveProductToFridge = '/fridge',
  GetProductsByProductTypeId = '/fridge/products/productType/:productTypeId',
}

export enum Storage {
  ProjectToken = 'token',
}
