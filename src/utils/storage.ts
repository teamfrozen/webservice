import {get, set, clear} from 'local-storage';

import {Storage} from '../constants/AppConfig';

export function getAuthToken(): string | null {
  return get(Storage.ProjectToken);
}

export function saveAuthToken(token: string): void {
  set(Storage.ProjectToken, token);
}

export function clearStorage() {
  clear();
}
