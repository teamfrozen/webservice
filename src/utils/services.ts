import {AxiosResponse} from 'axios';
import {User} from '@store/user';
import {saveAuthToken} from './storage';

export interface ResponseWithPayload<T> {
  payload: T;
}

export function extractPayload<T>(
  response: AxiosResponse<ResponseWithPayload<T>>,
): T {
  return response.data.payload;
}

export function saveAuthTokenToStorage(user: User): User {
  saveAuthToken(user.token);

  return user;
}
