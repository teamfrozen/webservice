export function upperFirst(string?: string): string {
  return string ? string[0].toUpperCase() + string.slice(1) : '';
}

export function replaceParam(string: string, param: string) {
  return string.replace(/:[^\/]+/, param);
}
