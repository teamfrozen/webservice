export type RequestState<T> =
  | RequestStateSuccess<T>
  | RequestStateLoading<T>
  | RequestStateInit
  | RequestStateError<T>;

export enum RequestStatus {
  Idle = 'idle',
  Loading = 'loading',
  Success = 'success',
  Error = 'error',
}

export interface RequestStateSuccess<T> {
  status: RequestStatus.Success;
  data: T;
  error: undefined;
}

export interface RequestStateLoading<T> {
  status: RequestStatus.Loading;
  data: T | undefined;
  error: undefined;
}

export interface RequestStateInit {
  status: RequestStatus.Idle;
  data: undefined;
  error: undefined;
}

export interface RequestStateError<T> {
  status: RequestStatus.Error;
  data: T | undefined;
  error: Error;
}

export function isRequestStatusSuccess<T>(
  state: RequestState<T>,
): state is RequestStateSuccess<T> {
  return state.status === RequestStatus.Success;
}

export function isRequestStatusLoading<T>(
  state: RequestState<T>,
): state is RequestStateLoading<T> {
  return state.status === RequestStatus.Loading;
}

export function isRequestStateError<T>(
  state: RequestState<T>,
): state is RequestStateError<T> {
  return state.status === RequestStatus.Error;
}

export function isRequestStateInit<T>(
  state: RequestState<T>,
): state is RequestStateInit {
  return state.status === RequestStatus.Idle;
}

export function createInitRequestState(): RequestStateInit {
  return {
    status: RequestStatus.Idle,
    data: undefined,
    error: undefined,
  };
}

export function createLoadingRequestState<T>(): RequestStateLoading<T> {
  return {
    status: RequestStatus.Loading,
    data: undefined,
    error: undefined,
  };
}
