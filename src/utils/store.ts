import {Effect, Store} from 'effector';
import {notifyError, notifySuccess} from '../store/notifications';
import {RequestState, RequestStatus} from './request';

export type Mapper<T, X, Y> = ({
  state,
  request,
  response,
}: {
  state: T | undefined;
  request: X;
  response: Y;
}) => T;

export function withSetState<T, X, Y>(
  store: Store<RequestState<T>>,
  effect: Effect<X, Y, Error>,
  mapper: Mapper<T, X, Y> = createDefaultMapper<T, X, Y>(),
): void {
  store.on(effect, ({data}) => {
    return {
      data,
      error: undefined,
      status: RequestStatus.Loading,
    };
  });

  store.on(effect.done, ({data}, {result, params}) => {
    return {
      error: undefined,
      status: RequestStatus.Success,
      data: mapper({
        state: data,
        request: params,
        response: result,
      }),
    };
  });

  store.on(effect.fail, ({data}, {error}) => {
    return {
      data,
      error,
      status: RequestStatus.Error,
    };
  });
}

export function withNotifications<X, Y>(
  effect: Effect<X, Y, Error>,
  {
    errorMessage,
    successMessage,
  }: {errorMessage?: string; successMessage?: string} = {},
) {
  effect.done.watch(() => {
    notifySuccess(successMessage || 'Success');
  });

  effect.fail.watch(({error}) => {
    notifyError(errorMessage || 'Error');
  });
}

function createDefaultMapper<T, X, Y>(): Mapper<T, X, Y> {
  return ({response}) => response as unknown as T;
}
