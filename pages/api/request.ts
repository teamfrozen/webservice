import {NextApiRequest, NextApiResponse} from 'next';
import axios, {AxiosError} from 'axios';

const api = axios.create({
  // baseURL: 'http://localhost:3001/api/v1',
  baseURL: 'http://51.250.2.17:3001/api/v1',
});

export default async function makeRequestToApiGateway(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  if (isPost(req) && hasBody(req)) {
    try {
      const {data} = await api.request(req.body);

      return res.json(data);
    } catch (error) {
      if (isAxiosError(error)) {
        return res.json({
          status: 'error',
          error: error.response,
        });
      }

      return res.json({status: 'error'});
    }
  }

  return res.json({status: 'ok'});
}

function isAxiosError(error: any): error is AxiosError {
  return error.isAxiosError;
}

function isPost(req: NextApiRequest) {
  return req.method === 'POST';
}

function hasBody(req: NextApiRequest) {
  return req.body;
}
