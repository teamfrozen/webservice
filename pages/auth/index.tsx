import {AuthHead} from '@atomic/pages/AuthPage/AuthHead';
import {AuthBody} from '@atomic/pages/AuthPage/AuthBody';
import {Notifier} from '@atomic/templates/Notifier/Notifier';
import {StyledEngineProvider} from '@mui/material/styles';
import {MuiThemeProvider} from '@context/MuiThemeProvider';
import {SnackbarProvider} from 'notistack';

export default function AuthRoute(): JSX.Element {
  return (
    <SnackbarProvider maxSnack={3}>
      <MuiThemeProvider>
        <StyledEngineProvider>
          <AuthHead />
          <AuthBody />
          <Notifier />
        </StyledEngineProvider>
      </MuiThemeProvider>
    </SnackbarProvider>
  );
}
