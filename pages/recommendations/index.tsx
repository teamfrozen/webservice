import {RecommendationsHead} from '@atomic/pages/RecommendationsPage/RecommendationsHead';
import {RecommendationsBody} from '@atomic/pages/RecommendationsPage/RecommendationsBody';
import {Notifier} from '@atomic/templates/Notifier/Notifier';
import {StyledEngineProvider} from '@mui/material/styles';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import {MuiThemeProvider} from '@context/MuiThemeProvider';
import {SnackbarProvider} from 'notistack';

export default function MainRoute(): JSX.Element {
  return (
    <SnackbarProvider maxSnack={3}>
      <MuiThemeProvider>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <StyledEngineProvider>
            <RecommendationsHead />
            <RecommendationsBody />
            <Notifier />
          </StyledEngineProvider>
        </LocalizationProvider>
      </MuiThemeProvider>
    </SnackbarProvider>
  );
}
