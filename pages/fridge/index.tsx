import {FridgeHead} from '@atomic/pages/FridgePage/FridgeHead';
import {FridgeBody} from '@atomic/pages/FridgePage/FridgeBody';
import {Notifier} from '@atomic/templates/Notifier/Notifier';
import {StyledEngineProvider} from '@mui/material/styles';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import {MuiThemeProvider} from '@context/MuiThemeProvider';
import {SnackbarProvider} from 'notistack';

export default function MainRoute(): JSX.Element {
  return (
    <SnackbarProvider maxSnack={3}>
      <MuiThemeProvider>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <StyledEngineProvider>
            <FridgeHead />
            <FridgeBody />
            <Notifier />
          </StyledEngineProvider>
        </LocalizationProvider>
      </MuiThemeProvider>
    </SnackbarProvider>
  );
}
