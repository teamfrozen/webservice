import {MainHead} from '@atomic/pages/MainPage/MainHead';
import {MainBody} from '@atomic/pages/MainPage/MainBody';
import {Notifier} from '@atomic/templates/Notifier/Notifier';
import {StyledEngineProvider} from '@mui/material/styles';
import {MuiThemeProvider} from '@context/MuiThemeProvider';
import {SnackbarProvider} from 'notistack';

export default function MainRoute(): JSX.Element {
  return (
    <SnackbarProvider maxSnack={3}>
      <MuiThemeProvider>
        <StyledEngineProvider>
          <MainHead />
          <MainBody />
          <Notifier />
        </StyledEngineProvider>
      </MuiThemeProvider>
    </SnackbarProvider>
  );
}
